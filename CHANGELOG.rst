##########
Change Log
##########
.. contents:: :depth: 2

*******
General
*******
Normal releases named after their respective (annotated) repository tag.

DELETED - 2015-11-18
=====================
This repository is no longer in use, below are changes before this repository
reintegrated into the parent repository (afstudeer_).
Added
-----
* Glossary (global?)
* Use version numbers based on parent repository and document type.
* WIP: Convert (and update) actual thesis report to rst.

Changed
-------
* Use branches per type of documentation
* Use separate files per section, and improve file-naming scheme
* Store figures in a global location (and link to them when needed)
* Store (generated) images when needed for pdf-builds

Depreciated
-----------

Removed
-------
* Obsoleted lyx files

Fixed
-----
* cross reference and linking errors.
* Do not build items that are not needed.

0.0.1_ - 2015-07-29
===================
Initial release after split from the afstudeer_ parent repository. Any changes
here are with respect to statusreport-v1_.

Added
-----
* (*statusreport*) List of research ideas.
* (*thesisreport*) Crude summary per paper, when needed.
* (*thesisreport*) Include citation library.
* Allow inclusion of IDE config files (subject to reconsideration).

Changed
-------
* Switch to restructured text (sphinx) for documents (WIP).
* Use character styles in lyx-documents (alongside ordinary bold, italic, etc).a
* Use basic class for lyx documents and split into more child documents.
* (*thesisreport*) Improve multiple sections.
* (*statusreport*) Improve overview of past work.
* Updated README's

Depreciated
-----------
* Lyx documents, due to switch to sphinx

Removed
-------
* Duplicate texts in documents

Fixed
-----
* (*diverse reports*) Various missing labels in documents
* Missing images. When applicable use updated versions.


.. Hyperlinks
.. _afstudeer: https://bitbucket.org/R1dO/afstudeer
.. _statusreport-v1: https://bitbucket.org/R1dO/afstudeer/commits/tag/statusreport-v1
.. _0.0.1: https://bitbucket.org/R1dO/afstudeer-docs/commits/tag/split-from-parent
.. _afstudeer: https://bitbucket.org/R1dO/afstudeer
